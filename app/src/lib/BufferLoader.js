class BufferLoader {

  constructor(context) {
    if(!context) {
      throw new Error('Send a Web Audio API context to BufferLoader');
    }
    this._context = context;
    this._audiopaths = [];
    this._audiofiles = [];
  }

  audiopaths(newAudiopaths) {
    // Operating as a getter for the property
    if(!newAudiopaths) {
      return this._audiopaths;
    }

    // Otherwise set the audio files
    if (newAudiopaths instanceof Array) {
      this._audiopaths = newAudiopaths.slice();
      this._audiofiles = newAudiopaths.slice().fill(0);
    } else {
      throw new Error('Expected an array of audiofiles, but that\'s not what I got.');
    }
    return this;
  }

  audiofiles() {
    return this._audiofiles.filter((f) => {
      return f.constructor.name === 'AudioBuffer';
    });
  }

  load(fn) {
    if (this._audiopaths.length < 1) {
      console.warn('Audiofiles are not populated');
    }
    this._audiopaths.map((path,index) => {
      return fetch(path)
        .then( response => response.arrayBuffer())
        .then( buffer => {
          return  this._context.decodeAudioData(buffer);
        })
        .then( data => {
          this._audiofiles[index] = data;
          const stillWorking = this._audiofiles.findIndex(v => v === 0);
          if (stillWorking === -1) {
            fn();
          }
          return true;
        })
        .catch(e => {
          console.warn(`Trouble loading or decoding: ${path}`);
          this._audiofiles[index] = e;
        });
    });
    return this;
  }
}

/*
.then(decoded => {
  this._audiofiles[index] = decoded;
  const stillWorking = this._audiofiles.findIndex(v => v === 0);
  if (stillWorking === -1) {
    fn();
  }
},(e) => {
  console.log(e);
})
*/

export default BufferLoader;